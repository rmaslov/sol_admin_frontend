import ViewTemplateCreateFormScheme from "../entity/ViewTemplateCreateFormScheme";
import ViewTemplateCreateEntity from "../entity/ViewTemplateCreateEntity";
import FTLSmartCreateDomainStore
  from "ftl-dashboards-templates/dist/domains/FTLSmartCreateDomain/stores/FTLSmartCreateDomainStore";
import ViewTemplateCreateMockRepository from "../../../../infrastructure/mock/viewTemplate/ViewTemplateCreateMockRepository";
import ViewTemplateCreateApiRepository from "../../../../infrastructure/api/viewTemplate/ViewTemplateCreateApiRepository";

const ViewTemplateCreateStore = FTLSmartCreateDomainStore<typeof ViewTemplateCreateFormScheme, ViewTemplateCreateEntity>({
  schema: ViewTemplateCreateFormScheme,
  repository: ViewTemplateCreateApiRepository
  // repository: ViewTemplateCreateMockRepository
})
export default ViewTemplateCreateStore
