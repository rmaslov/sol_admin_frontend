import {string} from "efform";

const ViewTemplateCreateFormScheme = {
    id: string(),
    title: string().required(),
    description: string(),
}

export default ViewTemplateCreateFormScheme
