interface ViewTemplateCreateEntity {
  id: string,
  title: string,
  description: string,
}

export default ViewTemplateCreateEntity
