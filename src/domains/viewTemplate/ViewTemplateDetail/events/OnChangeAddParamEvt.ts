import {createEvent} from "effector";
import ViewParamsType from "../entity/ViewParamsType";

export interface OnChangeAddParamProps {
    name: 'type' | 'valueString' | 'valueDate' | 'valueBool',
    value: string | boolean | ViewParamsType
}

const OnChangeAddParamEvt = () => {
    return createEvent<OnChangeAddParamProps>('OnChangeAddParam')
}


export default OnChangeAddParamEvt