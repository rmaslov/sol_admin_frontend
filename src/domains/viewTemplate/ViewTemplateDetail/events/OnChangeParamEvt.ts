import {createEvent} from "effector";
import ViewParamsType from "../entity/ViewParamsType";

export interface OnChangeParamProps {
    id: string,
    name: 'type' | 'valueString' | 'valueDate' | 'valueBool',
    value: string | boolean | ViewParamsType
}

const OnChangeParamEvt = () => {
    return createEvent<OnChangeParamProps>('OnChangeParamEvt')
}


export default OnChangeParamEvt