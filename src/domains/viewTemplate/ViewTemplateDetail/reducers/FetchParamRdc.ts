import ViewTemplateParamsStoreEntity from "../entity/ViewTemplateParamsStoreEntity";
import OnChangeAddParamEvt, {OnChangeAddParamProps} from "../events/OnChangeAddParamEvt";
import ViewParamsType from "../entity/ViewParamsType";
import ViewTemplateEntity from "../entity/ViewTemplateEntity";

const FetchParamRdc = (store:ViewTemplateParamsStoreEntity, payload: ViewTemplateEntity) => {
    const newStore = {...store}
    newStore.paramsList = payload.view.params
    return newStore
}

export default FetchParamRdc