import ViewTemplateParamsStoreEntity from "../entity/ViewTemplateParamsStoreEntity";
import OnChangeAddParamEvt, {OnChangeAddParamProps} from "../events/OnChangeAddParamEvt";
import ViewParamsType from "../entity/ViewParamsType";
import {OnChangeParamProps} from "../events/OnChangeParamEvt";

const ChangeParamRdc = (store:ViewTemplateParamsStoreEntity, payload: OnChangeParamProps) => {
    const newStore = {...store}
    for(let key in newStore.paramsList){
        if(newStore.paramsList[key].id == payload.id){
            if(payload.name == 'type'){
                newStore.paramsList[key].type = payload.value as ( 'SLOT_TIME' | 'NOTIFICATION' | 'DEADLINE_EXACT_DATE' | 'HAS_REPEAT' | 'DEADLINE_RELATIVE_DATE' | 'TASK_IS_OVERDUE' | 'CLOSED' | 'DEADLINE_CHANGED_FEW_TIMES')
            }
            if(payload.name == 'valueBool'){
                newStore.paramsList[key].valueBool = payload.value as boolean
            }

            if(payload.name == 'valueString'){
                newStore.paramsList[key].valueString = payload.value as string
            }

            if(payload.name == 'valueDate'){
                newStore.paramsList[key].valueDate = payload.value as string
            }
        }
    }


    return newStore
}

export default ChangeParamRdc