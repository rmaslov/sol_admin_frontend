import ViewTemplateParamsStoreEntity from "../entity/ViewTemplateParamsStoreEntity";
import OnChangeAddParamEvt, {OnChangeAddParamProps} from "../events/OnChangeAddParamEvt";
import ViewParamsType from "../entity/ViewParamsType";

const ChangeAddParamRdc = (store:ViewTemplateParamsStoreEntity, payload: OnChangeAddParamProps) => {
    const newStore = {...store}
    if(payload.name == 'type'){
        newStore.addParams.type = payload.value as ( 'SLOT_TIME' | 'NOTIFICATION' | 'DEADLINE_EXACT_DATE' | 'HAS_REPEAT' | 'DEADLINE_RELATIVE_DATE' | 'TASK_IS_OVERDUE' | 'CLOSED' | 'DEADLINE_CHANGED_FEW_TIMES')
    }
    if(payload.name == 'valueBool'){
        newStore.addParams.valueBool = payload.value as boolean
    }

    if(payload.name == 'valueString'){
        newStore.addParams.valueString = payload.value as string
    }

    if(payload.name == 'valueDate'){
        newStore.addParams.valueDate = payload.value as string
    }

    return newStore
}

export default ChangeAddParamRdc