import ViewParamsEntity from "./ViewTemplateParamsEntity";

interface ViewTemplateDetailEntity {
    id: string,
    ownerId?: string,
    title: string,
    description: string,
    createdFromViewId?: string,
    status: string,
    ownerType: string,
    language: string,
    addByDefault: boolean,
    canEdit: boolean,
    view: {
        icon: {
            data: string,
            type: string,
        },
        title: string,
        description: string,
        addedType: string,
        displayMode: string,
        sortType: string,
        viewType: string,
    },
}

export default ViewTemplateDetailEntity
