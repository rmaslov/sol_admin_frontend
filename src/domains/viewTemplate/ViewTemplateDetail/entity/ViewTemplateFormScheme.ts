import {array, boolean, number, string} from "efform";
import ViewParamsType from "./ViewParamsType";
const ViewTemplateFormScheme = {
    id: string(),
    ownerId: string(),
    title: string(),
    description: string(),
    createdFromViewId: string(),
    status: string('DRAFT'),
    ownerType: string('BY_ADMIN'),
    language: string('ENGLISH'),
    addByDefault: boolean(false),
    canEdit: boolean(false),
    view: {
        icon: {
            data: string(),
            type: string(),
        },
        title: string(),
        description: string(),
        addedType: string(),
        displayMode: string(),
        sortType: string(),
        viewType: string(),
    },
}

export default ViewTemplateFormScheme
