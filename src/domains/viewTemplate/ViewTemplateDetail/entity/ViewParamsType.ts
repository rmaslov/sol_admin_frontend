enum ViewParamsType {
    SLOT_TIME,
    NOTIFICATION,
    DEADLINE_EXACT_DATE,
    HAS_REPEAT,
    DEADLINE_RELATIVE_DATE,
    TASK_IS_OVERDUE,
    CLOSED,
    FROM_SPACE,
    DEADLINE_CHANGED_FEW_TIMES
}

export const ViewParamsTypeList = [
    {label: 'SLOT_TIME', value: 'SLOT_TIME'},
    {label: 'NOTIFICATION', value: 'NOTIFICATION'},
    {label: 'DEADLINE_EXACT_DATE', value: 'DEADLINE_EXACT_DATE'},
    {label: 'HAS_REPEAT', value: 'HAS_REPEAT'},
    {label: 'DEADLINE_RELATIVE_DATE', value: 'DEADLINE_RELATIVE_DATE'},
    {label: 'TASK_IS_OVERDUE', value: 'TASK_IS_OVERDUE'},
    {label: 'CLOSED', value: 'CLOSED'},
    {label: 'FROM_SPACE', value: 'FROM_SPACE'},
    {label: 'DEADLINE_CHANGED_FEW_TIMES', value: 'DEADLINE_CHANGED_FEW_TIMES'}
]

export default ViewParamsType