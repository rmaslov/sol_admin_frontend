import ViewParamsType from "./ViewParamsType";

interface ViewParamsEntity {
    id: string,
    type: 'SLOT_TIME' | 'NOTIFICATION' | 'DEADLINE_EXACT_DATE' | 'HAS_REPEAT' | 'DEADLINE_RELATIVE_DATE' | 'TASK_IS_OVERDUE' | 'CLOSED' | 'DEADLINE_CHANGED_FEW_TIMES',
    valueString: string,
    valueDate: string,
    valueBool: boolean
}

export default ViewParamsEntity