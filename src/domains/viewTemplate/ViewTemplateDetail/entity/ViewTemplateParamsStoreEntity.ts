import ViewTemplateParamsEntity from "./ViewTemplateParamsEntity";

interface ViewTemplateParamsStoreEntity {
    idTemplate: string,
    addParams: ViewTemplateParamsEntity,
    paramsList: ViewTemplateParamsEntity[]
}

export default ViewTemplateParamsStoreEntity