import {createEffect} from "effector/effector.umd";
import ViewTemplateAdminApiRepository from "../../../../infrastructure/api/viewTemplate/ViewTemplateAdminApiRepository";
import ViewTemplateEntity from "../entity/ViewTemplateEntity";
import ViewParamsEntity from "../entity/ViewTemplateParamsEntity";

export interface FetchParamsToViewTemplateProps {
    idTemplate: string
}

const FetchParamsToViewTemplateFx = () => {
    const FetchParamsToViewTemplateFx = createEffect<FetchParamsToViewTemplateProps, ViewTemplateEntity>({
        name: 'FetchParamsToViewTemplateFx'
    })

    FetchParamsToViewTemplateFx.use( (params: FetchParamsToViewTemplateProps) => {
        const promise = new Promise<ViewTemplateEntity>(async (resolve, reject) => {
            const result = await ViewTemplateAdminApiRepository.fetchParams(params.idTemplate)
            resolve(result.result)
        });
        return  promise;
    })

    return FetchParamsToViewTemplateFx
}


export default FetchParamsToViewTemplateFx