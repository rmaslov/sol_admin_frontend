import {createEffect} from "effector/effector.umd";
import ViewTemplateAdminApiRepository from "../../../../infrastructure/api/viewTemplate/ViewTemplateAdminApiRepository";
import ViewTemplateEntity from "../entity/ViewTemplateEntity";
import ViewParamsEntity from "../entity/ViewTemplateParamsEntity";

export interface AddParamsToViewTemplateProps {
    idTemplate: string, param: ViewParamsEntity
}

const AddParamsToViewTemplateFx = () => {
    const AddParamsToViewTemplateFx = createEffect<AddParamsToViewTemplateProps, ViewTemplateEntity>({
        name: 'AddParamsToViewTemplateFx'
    })

    AddParamsToViewTemplateFx.use( (params: AddParamsToViewTemplateProps) => {
        const promise = new Promise<ViewTemplateEntity>(async (resolve, reject) => {
            const result = await ViewTemplateAdminApiRepository.addParams(params.idTemplate, params.param)
            resolve(result.result)
        });
        return  promise;
    })

    return AddParamsToViewTemplateFx
}


export default AddParamsToViewTemplateFx