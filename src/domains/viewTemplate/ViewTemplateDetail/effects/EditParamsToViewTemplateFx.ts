import {createEffect} from "effector/effector.umd";
import ViewTemplateAdminApiRepository from "../../../../infrastructure/api/viewTemplate/ViewTemplateAdminApiRepository";
import ViewTemplateEntity from "../entity/ViewTemplateEntity";
import ViewParamsEntity from "../entity/ViewTemplateParamsEntity";

export interface EditParamsToViewTemplateProps {
    idTemplate: string, param: ViewParamsEntity
}

const EditParamsToViewTemplateFx = () => {
    const EditParamsToViewTemplateFx = createEffect<EditParamsToViewTemplateProps, ViewTemplateEntity>({
        name: 'EditParamsToViewTemplateFx'
    })

    EditParamsToViewTemplateFx.use( (params: EditParamsToViewTemplateProps) => {
        const promise = new Promise<ViewTemplateEntity>(async (resolve, reject) => {
            const result = await ViewTemplateAdminApiRepository.editParams(params.idTemplate, params.param)
            resolve(result.result)
        });
        return  promise;
    })

    return EditParamsToViewTemplateFx
}


export default EditParamsToViewTemplateFx