import {createEffect} from "effector/effector.umd";
import ViewTemplateAdminApiRepository from "../../../../infrastructure/api/viewTemplate/ViewTemplateAdminApiRepository";
import ViewTemplateEntity from "../entity/ViewTemplateEntity";
import ViewParamsEntity from "../entity/ViewTemplateParamsEntity";

export interface DeleteParamsToViewTemplateProps {
    idTemplate: string, id: string
}

const DeleteParamsToViewTemplateFx = () => {
    const DeleteParamsToViewTemplateFx = createEffect<DeleteParamsToViewTemplateProps, ViewTemplateEntity>({
        name: 'DeleteParamsToViewTemplateFx'
    })

    DeleteParamsToViewTemplateFx.use( (params: DeleteParamsToViewTemplateProps) => {
        const promise = new Promise<ViewTemplateEntity>(async (resolve, reject) => {
            const result = await ViewTemplateAdminApiRepository.deleteParams(params.idTemplate, params.id)
            resolve(result.result)
        });
        return  promise;
    })

    return DeleteParamsToViewTemplateFx
}


export default DeleteParamsToViewTemplateFx