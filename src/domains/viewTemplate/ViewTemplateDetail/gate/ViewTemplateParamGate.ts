import {createGate} from "effector-react";

const ViewTemplateParamGate = () => {
    const viewTemplateParamGate = createGate<string>()
    return viewTemplateParamGate
}

export default ViewTemplateParamGate