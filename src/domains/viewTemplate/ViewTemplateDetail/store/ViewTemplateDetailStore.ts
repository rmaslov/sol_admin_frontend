import ViewTemplateFormScheme from "../entity/ViewTemplateFormScheme";
import {FTLSmartDetailDomainStore} from "ftl-dashboards-templates";
import ViewTemplateDetailEntity from "../entity/ViewTemplateDetailEntity";
import ViewTemplateDetailApiRepository from "../../../../infrastructure/api/viewTemplate/ViewTemplateDetailApiRepository";
import ViewTemplateDetailMockRepository from "../../../../infrastructure/mock/viewTemplate/ViewTemplateDetailMockRepository";

const ViewTemplateDetailStore = FTLSmartDetailDomainStore<typeof ViewTemplateFormScheme, ViewTemplateDetailEntity>({
  schema: ViewTemplateFormScheme,
  repository: ViewTemplateDetailApiRepository
  // repository: ViewTemplateDetailMockRepository
})

export default ViewTemplateDetailStore
