import {FTLSmartTableStore} from "ftl-dashboards-templates";
import {createStore} from "effector";
import ViewTemplateParamsStoreEntity from "../entity/ViewTemplateParamsStoreEntity";
import ViewTemplateParamsEntity from "../entity/ViewTemplateParamsEntity";
import ViewParamsType from "../entity/ViewParamsType";
import OnChangeAddParamEvt from "../events/OnChangeAddParamEvt";
import ChangeAddParamRdc from "../reducers/ChangeAddParamRdc";
import AddParamsToViewTemplateFx from "../effects/AddParamsToViewTemplateFx";
import EditParamsToViewTemplateFx from "../effects/EditParamsToViewTemplateFx";
import DeleteParamsToViewTemplateFx from "../effects/DeleteParamsToViewTemplateFx";
import FetchParamRdc from "../reducers/FetchParamRdc";
import ViewTemplateParamGate from "../gate/ViewTemplateParamGate";
import FetchParamsToViewTemplateFx from "../effects/FetchParamsToViewTemplateFx";
import OnChangeParamEvt from "../events/OnChangeParamEvt";
import ChangeParamRdc from "../reducers/ChangeParamRdc";


const ViewTemplateParamsStore = () => {

    const defaultState: ViewTemplateParamsStoreEntity = {
        idTemplate: '',
        addParams: {
            id: '',
            type: 'CLOSED',
            valueString: '',
            valueDate: '',
            valueBool: false
        },
        paramsList: []
    }
    const store = createStore(defaultState)
    const onChangeAddParamEvt = OnChangeAddParamEvt()
    const onChangeParamEvt = OnChangeParamEvt()
    const fetchParamsToViewTemplateFx = FetchParamsToViewTemplateFx()
    const addParamsToViewTemplateFx = AddParamsToViewTemplateFx()
    const editParamsToViewTemplateFx = EditParamsToViewTemplateFx()
    const deleteParamsToViewTemplateFx = DeleteParamsToViewTemplateFx()
    const gate = ViewTemplateParamGate()

    store.watch((store) => {
        console.log(store)
    })

    store.on(onChangeAddParamEvt, ChangeAddParamRdc)
    store.on(onChangeParamEvt, ChangeParamRdc)
    store.on(fetchParamsToViewTemplateFx.doneData, FetchParamRdc)
    store.on(addParamsToViewTemplateFx.doneData, FetchParamRdc)
    store.on(editParamsToViewTemplateFx.doneData, FetchParamRdc)
    store.on(deleteParamsToViewTemplateFx.doneData, FetchParamRdc)
    store.watch(gate.open, (state) => fetchParamsToViewTemplateFx({idTemplate: state.idTemplate}))
    store.on(gate.open, (state, id: string) => {
        return {...state, idTemplate: id}
    })

    return {
        store: store,
        onChangeAddParamEvt: onChangeAddParamEvt,
        onChangeParamEvt: onChangeParamEvt,
        addParamFx: addParamsToViewTemplateFx,
        editParamFx: editParamsToViewTemplateFx,
        deleteParamFx: deleteParamsToViewTemplateFx,
        gate: gate
    }
}

export default ViewTemplateParamsStore
