import ViewEntity from "../../base/entity/ViewEntity";

type ViewTemplateListEntity = {
    id: string,
    ownerId: string,
    title: string,
    description: string,
    createdFromViewId: string,
    status: string,
    ownerType: string,
    language: string,
    view: ViewEntity,
    addByDefault: string,
    canEdit: string,
}

export default ViewTemplateListEntity
