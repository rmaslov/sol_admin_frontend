import {FTLSmartTableStore} from "ftl-dashboards-templates";
import ViewTemplateListEntity from "../entity/ViewTemplateListEntity";
import ViewTemplateListApiRepository from "../../../../infrastructure/api/viewTemplate/ViewTemplateListApiRepository";
import ViewTemplateListMockRepository
    from "../../../../infrastructure/mock/viewTemplate/ViewTemplateListMockRepository";
import {FTLButton} from "ftl-uikit";
import ViewTemplateAdminApiRepository from "../../../../infrastructure/api/viewTemplate/ViewTemplateAdminApiRepository";

const ViewTemplateListStore = FTLSmartTableStore<ViewTemplateListEntity>({
    metadata: [
        {
            id: "title",
            headerName: "Title",
            renderCell: (entity) => entity.title
        },
        {
            id: "status",
            headerName: "status",
            renderCell: (entity) => entity.status
        },
        {
            id: "ownerType",
            headerName: "ownerType",
            renderCell: (entity) => entity.ownerType
        },
        {
            id: "actions",
            headerName: "Actions",
            renderCell: (entity) => {
                return (<div>
                    <FTLButton onClick={async () => {
                        await ViewTemplateAdminApiRepository.deleteTemplate(entity.id)
                    }}>delete</FTLButton>
                </div>)
            }
        }

    ],
    repo: ViewTemplateListApiRepository
    // repo: ViewTemplateListMockRepository
})

export default ViewTemplateListStore
