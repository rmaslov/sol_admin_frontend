interface EditParamRequest{
    idTemplate: string,
    id: string,
    type: string,
    valueString: string,
    valueDate: string,
    valueBool: boolean,
}

export default EditParamRequest
