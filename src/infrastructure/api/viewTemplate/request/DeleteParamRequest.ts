interface DeleteParamRequest {
    idTemplate: string,
    id: string,
}

export default DeleteParamRequest
