interface AddParamRequest{
    idTemplate: string,
    type: string,
    valueString: string,
    valueDate: string,
    valueBool: boolean,
}

export default AddParamRequest