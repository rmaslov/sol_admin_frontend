import {BasicResponse, ListResponse} from "ftl-core";
import {API_BASE_URL} from "ftl-dashboards-templates/dist/infrastructure/api/auth/routers/AuthRouter";
import BaseApiRepository from "../base/BaseApiRepository";
import ViewTemplateListEntity from "../../../domains/viewTemplate/ViewTemplateList/entity/ViewTemplateListEntity";


const ViewTemplateListApiRepository = {
    fetchData: (skip: number, pageSize: number, params?: Map<string, string>, sortName?: string, sortDirection?: string) => {
        let paramQuery = ""
        if (params != undefined) {
            params!.forEach((value, key) => {
                paramQuery += `&${key}=${value}`
            })
        }
        let url = `${API_BASE_URL}/api/v1/view-template?limit=${pageSize}&offset=${skip}${paramQuery}`;
        if (sortName != undefined && sortName != "") {
            url = `${url}&sortName=${sortName}`
        }

        if (sortDirection != undefined && sortDirection != "") {
            url = `${url}&sortDirection=${sortDirection.toUpperCase()}`
        }

        return new Promise<ListResponse<ViewTemplateListEntity>>(async (resolve) => {
            const result = await BaseApiRepository.get<ListResponse<ViewTemplateListEntity>>(url)
            resolve(result.data)
        })
    }
}

export default ViewTemplateListApiRepository
