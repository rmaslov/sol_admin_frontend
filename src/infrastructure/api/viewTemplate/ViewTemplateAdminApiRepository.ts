import {BasicResponse} from "ftl-core";
import ViewTemplateDetailEntity from "../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateDetailEntity";
import {API_BASE_URL} from "ftl-dashboards-templates/dist/infrastructure/api/auth/routers/AuthRouter";
import BaseApiRepository from "../base/BaseApiRepository";
import AddParamRequest from "./request/AddParamRequest";
import EditParamRequest from "./request/EditParamRequest";
import DeleteParamRequest from "./request/DeleteParamRequest";
import ViewParamsEntity from "../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateParamsEntity";
import ViewTemplateEntity from "../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateEntity";

const ViewTemplateAdminApiRepository = {
    bulkAddByAdmin: (entity: ViewTemplateDetailEntity) => {
        return new Promise<BasicResponse<string>>(async (resolve) => {
            const request = {
                idTemplate: entity.id
            }

            const result = await BaseApiRepository
                .post<BasicResponse<string>>(
                    `${API_BASE_URL}/api/v1/view-template/${entity.id}/bulk-add-by-admin`,
                    request)

            resolve(result.data)
        })
    },

    fetchParams: (idTemplate: string) => {
        return new Promise<BasicResponse<ViewTemplateEntity>>(async (resolve) => {
            const result = await BaseApiRepository
                .get<BasicResponse<ViewTemplateEntity>>(
                    `${API_BASE_URL}/api/v1/view-template/${idTemplate}`)

            const vt = result.data.result
            if (!vt.ownerId) vt.ownerId = ""
            if (!vt.createdFromViewId) vt.createdFromViewId = ""

            resolve({
                result: {
                    ...vt,
                    view: {
                        ...vt.view,

                        title: vt.view.title == null ? '' : vt.view.title,
                        description: vt.view.description == null ? "" : vt.view.description,
                        icon: {
                            ...vt.view.icon,
                            data: vt.view.icon.data == null ? "" : vt.view.icon.data
                        },
                        sortType: vt.view.sortType == null ? 'LIST' : vt.view.sortType,

                    }
                }
            })
        })
    },

    addParams: (idTemplate: string, param: ViewParamsEntity) => {
        return new Promise<BasicResponse<ViewTemplateEntity>>(async (resolve) => {
            const request: AddParamRequest = {
                idTemplate: idTemplate,
                type: param.type,
                valueString: param.valueString,
                valueDate: param.valueDate,
                valueBool: param.valueBool,
            }

            const result = await BaseApiRepository
                .post<BasicResponse<ViewTemplateEntity>>(
                    `${API_BASE_URL}/api/v1/view-template/${idTemplate}/param/add`,
                    request)

            const vt = result.data.result
            if (!vt.ownerId) vt.ownerId = ""
            if (!vt.createdFromViewId) vt.createdFromViewId = ""

            resolve({
                result: {
                    ...vt,
                    view: {
                        ...vt.view,

                        title: vt.view.title == null ? '' : vt.view.title,
                        description: vt.view.description == null ? "" : vt.view.description,
                        icon: {
                            ...vt.view.icon,
                            data: vt.view.icon.data == null ? "" : vt.view.icon.data
                        },
                        sortType: vt.view.sortType == null ? 'LIST' : vt.view.sortType,

                    }
                }
            })
        })
    },
    editParams: (idTemplate: string, param: ViewParamsEntity) => {
        return new Promise<BasicResponse<ViewTemplateEntity>>(async (resolve) => {
            const request: EditParamRequest = {
                idTemplate: idTemplate,
                id: param.id,
                type: param.type,
                valueString: param.valueString,
                valueDate: param.valueDate,
                valueBool: param.valueBool,
            }

            const result = await BaseApiRepository
                .post<BasicResponse<ViewTemplateEntity>>(
                    `${API_BASE_URL}/api/v1/view-template/${idTemplate}/param/edit`,
                    request)

            const vt = result.data.result
            if (!vt.ownerId) vt.ownerId = ""
            if (!vt.createdFromViewId) vt.createdFromViewId = ""

            resolve({
                result: {
                    ...vt,
                    view: {
                        ...vt.view,

                        title: vt.view.title == null ? '' : vt.view.title,
                        description: vt.view.description == null ? "" : vt.view.description,
                        icon: {
                            ...vt.view.icon,
                            data: vt.view.icon.data == null ? "" : vt.view.icon.data
                        },
                        sortType: vt.view.sortType == null ? 'LIST' : vt.view.sortType,
                    }
                }
            })
        })
    },
    deleteParams: (idTemplate: string, paramId: string) => {
        return new Promise<BasicResponse<ViewTemplateEntity>>(async (resolve) => {
            const request: DeleteParamRequest = {
                idTemplate: idTemplate,
                id: paramId,
            }

            const result = await BaseApiRepository
                .post<BasicResponse<ViewTemplateEntity>>(
                    `${API_BASE_URL}/api/v1/view-template/${idTemplate}/param/delete`,
                    request)

            const vt = result.data.result
            if (!vt.ownerId) vt.ownerId = ""
            if (!vt.createdFromViewId) vt.createdFromViewId = ""

            resolve({
                result: {
                    ...vt,
                    view: {
                        ...vt.view,

                        title: vt.view.title == null ? '' : vt.view.title,
                        description: vt.view.description == null ? "" : vt.view.description,
                        icon: {
                            ...vt.view.icon,
                            data: vt.view.icon.data == null ? "" : vt.view.icon.data
                        },
                        sortType: vt.view.sortType == null ? 'LIST' : vt.view.sortType,
                    }
                }
            })
        })
    },
    deleteTemplate: (idTemplate: string) => {
        return new Promise<BasicResponse<String>>(async (resolve) => {
            const result = await BaseApiRepository
                .delete<BasicResponse<String>>(
                    `${API_BASE_URL}/api/v1/view-template/${idTemplate}`)


            resolve(result.data)
        })
    },
}

export default ViewTemplateAdminApiRepository
