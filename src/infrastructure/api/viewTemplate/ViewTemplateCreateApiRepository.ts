import {BasicResponse} from "ftl-core";
import ViewTemplateCreateEntity from "../../../domains/viewTemplate/ViewTemplateCreate/entity/ViewTemplateCreateEntity";
import {API_BASE_URL} from "ftl-dashboards-templates/dist/infrastructure/api/auth/routers/AuthRouter";
import BaseApiRepository from "../base/BaseApiRepository";
import SmartCreateRepository
    from "ftl-dashboards-templates/dist/domains/FTLSmartCreateDomain/repositories/SmartCreateRepository";

const ViewTemplateCreateApiRepository: SmartCreateRepository<ViewTemplateCreateEntity> = {

    create: (entity: ViewTemplateCreateEntity) => {

        return new Promise<BasicResponse<ViewTemplateCreateEntity>>(async (resolve) => {
            const result = await BaseApiRepository
                .post<BasicResponse<ViewTemplateCreateEntity>>(
                    `${API_BASE_URL}/api/v1/view-template`,
                    entity)
            resolve({
                result: result.data.result
            })
        })
    }
}

export default ViewTemplateCreateApiRepository
