import {BasicResponse} from "ftl-core";
import SmartDetailRepository
    from "ftl-dashboards-templates/dist/domains/FTLSmartDetailDomain/repositories/SmartDetailRepository";
import ViewTemplateDetailEntity from "../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateDetailEntity";
import {API_BASE_URL} from "ftl-dashboards-templates/dist/infrastructure/api/auth/routers/AuthRouter";
import BaseApiRepository from "../base/BaseApiRepository";

const ViewTemplateDetailApiRepository: SmartDetailRepository<ViewTemplateDetailEntity> = {
    fetch: (id: string) => {
        return new Promise<BasicResponse<ViewTemplateDetailEntity>>((async resolve => {
            const result = await BaseApiRepository
                .get<BasicResponse<ViewTemplateDetailEntity>>(`${API_BASE_URL}/api/v1/view-template/${id}`)

            const vt = result.data.result
            if (!vt.ownerId) vt.ownerId = ""
            if (!vt.createdFromViewId) vt.createdFromViewId = ""

            resolve({
                result: {
                    ...vt,
                    view: {
                        ...vt.view,

                        title: vt.view.title == null ? '' : vt.view.title,
                        description: vt.view.description == null ? "" : vt.view.description,
                        icon: {
                            ...vt.view.icon,
                            data: vt.view.icon.data == null ? "" : vt.view.icon.data
                        },
                        sortType: vt.view.sortType == null ? 'LIST' : vt.view.sortType,
                    }
                }
            })
        }));
    },
    save: (entity: ViewTemplateDetailEntity) => {
        return new Promise<BasicResponse<ViewTemplateDetailEntity>>(async (resolve) => {
            const request = {
                title: entity.title,
                description: entity.description,
                viewTitle: entity.view.title,
                viewDescription: entity.view.description,
                iconEmoji: entity.view.icon.data,
                addedType: entity.view.addedType,
                displayMode: entity.view.displayMode,
                sortType: entity.view.sortType,
                canEdit: entity.canEdit,
                viewType: entity.view.viewType,
                addByDefault: entity.addByDefault,
                status: entity.status,
                idTemplate: entity.id
            }

            const result = await BaseApiRepository
                .put<BasicResponse<ViewTemplateDetailEntity>>(
                    `${API_BASE_URL}/api/v1/view-template/${entity.id}`,
                    request)

            const vt = result.data.result
            if (!vt.ownerId) vt.ownerId = ""
            if (!vt.createdFromViewId) vt.createdFromViewId = ""

            resolve({
                result: {
                    ...vt,
                    view: {
                        ...vt.view,

                        title: vt.view.title == null ? '' : vt.view.title,
                        description: vt.view.description == null ? "" : vt.view.description,
                        icon: {
                            ...vt.view.icon,
                            data: vt.view.icon.data == null ? "" : vt.view.icon.data
                        },
                        sortType: vt.view.sortType == null ? 'LIST' : vt.view.sortType,
                    }
                }
            })
        })
    },


}

export default ViewTemplateDetailApiRepository
