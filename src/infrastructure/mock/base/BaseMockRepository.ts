function baseMockRepository<Response>(code: number, response: Response) {
    return new Promise<Response>((resolve) => {
        resolve(response)
    })
}

function generate(count: number) {
    const words: string[] = []

    words.push("escapology")
    words.push("brightwork")
    words.push("verkrampte")
    words.push("protectrix")
    words.push("nudibranch")
    words.push("grandchild")
    words.push("newfangled")
    words.push("flugelhorn")
    words.push("mythologer")
    words.push("pluperfect")
    words.push("jellygraph")
    words.push("quickthorn")
    words.push("rottweiler")
    words.push("technician")
    words.push("cowpuncher")
    words.push("middlebrow")
    words.push("jackhammer")
    words.push("triphthong")
    words.push("wunderkind")
    words.push("dazzlement")
    words.push("jabberwock")
    words.push("witchcraft")
    words.push("pawnbroker")
    words.push("thumbprint")
    words.push("motorcycle")
    words.push("cryptogram")
    words.push("torchlight")
    words.push("bankruptcy")

    const result: string[] = []
    for (let i = 0; i < count; i++) {
        const rnd = Math.ceil(Math.random() * words.length)
        result.push(words[rnd])
    }

    return result.join(" ")
}

export default {
    execute: baseMockRepository,
    generate: generate
}
