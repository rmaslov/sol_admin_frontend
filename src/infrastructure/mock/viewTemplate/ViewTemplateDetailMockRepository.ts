import {BasicResponse} from "ftl-core";
import SmartDetailRepository
    from "ftl-dashboards-templates/dist/domains/FTLSmartDetailDomain/repositories/SmartDetailRepository";
import ViewTemplateDetailEntity from "../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateDetailEntity";
import BaseMockRepository from "../base/BaseMockRepository";
import {string} from "efform";


const ViewTemplateDetailMockRepository: SmartDetailRepository<ViewTemplateDetailEntity> = {
    fetch: (id: string) => BaseMockRepository.execute<BasicResponse<ViewTemplateDetailEntity>>(200, {
        result: {
            id: 'field',
            ownerId: 'field',
            title: 'field',
            description: 'field',
            createdFromViewId: 'field',
            status: 'field',
            ownerType: 'field',
            language: 'field',
            addByDefault: false,
            canEdit: false,
            view: {
                icon: {
                    data: 'string()',
                    type: 'string()',
                },
                title: 'string()',
                description: 'string()',
                addedType: 'string()',
                displayMode: 'string()',
                sortType: 'string()',
                viewType: 'string()'
                // params: ParamsScheme
            },
        }
    }),
    save: (entity: ViewTemplateDetailEntity) => BaseMockRepository.execute<BasicResponse<ViewTemplateDetailEntity>>(200, {
        result: entity
    })


}

export default ViewTemplateDetailMockRepository
