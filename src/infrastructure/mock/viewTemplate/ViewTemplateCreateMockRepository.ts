import {BasicResponse} from "ftl-core";
import ViewTemplateCreateEntity from "../../../domains/viewTemplate/ViewTemplateCreate/entity/ViewTemplateCreateEntity"
import BaseMockRepository from "../base/BaseMockRepository";
import SmartCreateRepository
    from "ftl-dashboards-templates/dist/domains/FTLSmartCreateDomain/repositories/SmartCreateRepository";


const ViewTemplateCreateMockRepository: SmartCreateRepository<ViewTemplateCreateEntity> = {
    create: (entity: ViewTemplateCreateEntity) => {
        alert('create did execute')
        return BaseMockRepository.execute<BasicResponse<ViewTemplateCreateEntity>>(200, {
            result: entity
        })
    }
}

export default ViewTemplateCreateMockRepository
