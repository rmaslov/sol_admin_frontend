import {ListResponse} from "ftl-core";
import BaseMockRepository from "../base/BaseMockRepository";
import ViewTemplateListEntity from "../../../domains/viewTemplate/ViewTemplateList/entity/ViewTemplateListEntity";


const ViewTemplateListMockRepository = {
    fetchData: (skip: number, pageSize: number, params?: Map<string, string>, sortName?: string, sortDirection?: string) => {
        const items: ViewTemplateListEntity[] = []
        for (let i = 0; i < pageSize; i++) {
            items.push({
                id: 'field',
                ownerId: 'field',
                title: 'field',
                description: 'field',
                createdFromViewId: 'field',
                status: 'field',
                ownerType: 'field',
                language: 'field',
                addByDefault: 'field',
                canEdit: 'field',
                view: {
                    icon: {
                        data: 'string()',
                        type: 'string()',
                    },
                    title: 'string()',
                    description: 'string()',
                    addedType: 'string()',
                    displayMode: 'string()',
                    sortType: 'string()',
                    // params: ParamsScheme
                },
            })
        }

        return BaseMockRepository.execute<ListResponse<ViewTemplateListEntity>>(200, {
            result: {
                count: 100,
                items: items
            }
        })
    }
}

export default ViewTemplateListMockRepository
