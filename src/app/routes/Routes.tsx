import React, {lazy, ReactNode, Suspense} from "react"
import {Route, Switch} from "react-router-dom"
import {ErrorContainer, FTLErrorBoundary, FTLErrorPage, FTLLoader} from "ftl-uikit"
import {errorPageService, history, logOutAction, PrivateRoute} from "ftl-core"
import {
    FTLBaseLayout,
    FTLCredentialDetailPage,
    FTLCredentialListPage,
    FTLMainLayoutErrorBoundaryComponent
} from "ftl-dashboards-templates";
import FTLMainLayoutBaseBoundaryProps
    from "ftl-dashboards-templates/dist/app/layouts/props/FTLMainLayoutBaseBoundaryProps";

//Profile
const ProfilePage = lazy(() => import("ftl-dashboards-templates/dist/app/templates/FTLProfilePage"))
const FTLInitAdminPage = lazy(() => import('ftl-dashboards-templates/dist/app/templates/FTLInitAdminPage'))

/**
 * VIEW TEMPLATE
 */
const ViewTemplateListPage = lazy(() => import('../pages/viewTemplate/ViewTemplateList/page/ViewTemplateListPage'))
const ViewTemplateCreatePage = lazy(() => import('../pages/viewTemplate/ViewTemplateCreate/page/ViewTemplateCreatePage'))
const ViewTemplateDetailPage = lazy(() => import('../pages/viewTemplate/ViewTemplateDetail/page/ViewTemplateDetailPage'))

export const Routes = ({
                           AuthClient,
                           toHomeUrl,
                           project,
                           StoreProviderProps,
                           children,
                           BaseLayoutProps
                       }: FTLMainLayoutBaseBoundaryProps) => {
    const BaseLayout = (title: string, childrenPage: ReactNode) => {
        return (<Suspense fallback={<FTLLoader height="50vh" color="dark"/>}>
            <FTLBaseLayout
                AuthClient={AuthClient}
                toHomeUrl={toHomeUrl}
                title={title}
                onSignOut={() =>
                    StoreProviderProps.store.dispatch(logOutAction)
                }
                logo={project.logo?.header}
                {...BaseLayoutProps}
            >
                <FTLErrorBoundary location={history.location}>
                    <ErrorContainer basePath={toHomeUrl}>
                        <ErrorContainer
                            service={errorPageService}
                            basePath={toHomeUrl}
                        >
                            {childrenPage}
                        </ErrorContainer>
                    </ErrorContainer>
                </FTLErrorBoundary>
            </FTLBaseLayout>
        </Suspense>)
    }

    return (
        <Switch>
            <PrivateRoute exact path={"/dashboard"}>
                {BaseLayout("Credential", <div>Welcome to Sol.app!</div>)}
            </PrivateRoute>

            <PrivateRoute exact path={"/credential"}>
                {BaseLayout("Credentials", <FTLCredentialListPage></FTLCredentialListPage>)}
            </PrivateRoute>
            <PrivateRoute exact path={"/credential/:id"}>
                {BaseLayout("Credential", <FTLCredentialDetailPage></FTLCredentialDetailPage>)}
            </PrivateRoute>

            <PrivateRoute exact path={"/view-template"}>
                {BaseLayout("View Template", <ViewTemplateListPage/>)}
            </PrivateRoute>
            <PrivateRoute exact path={"/view-template/new"}>
                {BaseLayout("Create View Template", <ViewTemplateCreatePage/>)}
            </PrivateRoute>
            <PrivateRoute exact path={"/view-template/:id"}>
                {BaseLayout("View Template", <ViewTemplateDetailPage/>)}
            </PrivateRoute>

            <Route exact path="/init-admin">
                <FTLMainLayoutErrorBoundaryComponent>
                    <Suspense fallback={<FTLLoader height="50vh" color="dark"/>}>
                        <FTLInitAdminPage redirectUrl={'/dashboard'}/>
                    </Suspense>
                </FTLMainLayoutErrorBoundaryComponent>
            </Route>

            <PrivateRoute exact path={["*", "/404"]}>
                <FTLErrorPage
                    title="Страницы не существует"
                    message="Наверное, вы перешли по неправильной ссылке."
                />
            </PrivateRoute>
        </Switch>
    )
}
