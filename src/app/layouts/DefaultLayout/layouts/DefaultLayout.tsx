import DefaultProps from "../props/DefaultProps";
import HeaderComponent from "../../../components/HeaderComponent/component/HeaderComponent";

const DefaultLayout = (props: DefaultProps) => {
    return (<div>
        <HeaderComponent/>
        {props.children}
    </div>)
}
export default DefaultLayout