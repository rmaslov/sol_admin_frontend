import {createStore} from "effector";
import getPostsByIds from "./GetPostsByIds";

export interface PostState {
    title: string,
    body: string
}

export interface PostGroupsState {
    id:number,
    posts:[PostState]
}
const defaultState: PostGroupsState[] = []

const reducer = (state = defaultState, action:any) => {
    return[
        ...state,
        ...action.result,
    ]
}

const postGroupsStore = createStore(defaultState)
    .on(getPostsByIds.done, reducer)

export default postGroupsStore;