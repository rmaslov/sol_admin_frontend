import {createEffect, forward} from "effector";
import getAllId from "./GetAllId";

const getPostsByIds = createEffect({
    handler: (ids:number[]) => Promise.all(ids.map(
        async id => {
            const res = await fetch(
                `https://jsonplaceholder.typicode.com/posts?userId=${id}`
            )
            const posts = await res.json()
            return {id, posts}
        }
    ))
})

forward({
    from: getAllId.done.map(result => result.result),
    to: getPostsByIds,
})

export default getPostsByIds