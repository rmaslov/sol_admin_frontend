import {useList} from "effector-react";
import postGroupsStore, {PostGroupsState} from "./PostGroupsStore";

function EffectsSequenceComponent() {
    return useList(postGroupsStore, (renderItem: PostGroupsState) => {
            return renderItem.posts ? renderItem.posts.map(({title, body}) => (
                <div key={title}>
                    {title}
                    <br/>
                    {body}
                    <hr/>
                </div>
            )) : (<div></div>)
        }
    )
}

export default EffectsSequenceComponent


