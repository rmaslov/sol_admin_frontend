import {useGate, useStore} from "effector-react";
import React from 'react';
import SampleCompGate from "./SampleCompGate";
import myVerySideEffectFx from "./MyVerySideEffectFx";

const Loading = () => (
    <div>I am loading huh?</div>
)
const ProfileForm = () => (
    <form>
        <input type='text' placeholder='my little input' />
        <input type='submit' />
    </form>
)

const SampleComp = () => {
    useGate(SampleCompGate) // instead of use Effect

    const loading = useStore(myVerySideEffectFx.pending)
    console.log(loading)

    return loading
        ? <Loading />
        : <ProfileForm />
}

export default SampleComp