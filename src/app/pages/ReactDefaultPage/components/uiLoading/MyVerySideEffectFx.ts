import {createEffect, forward} from "effector";
import SampleCompGate from "./SampleCompGate";

const myVerySideEffectFx = createEffect()

myVerySideEffectFx.use(async () => {
    const promise = new Promise<void>((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, 1000)
    });
    await promise;
})

myVerySideEffectFx.done.watch(() => console.log(' iam done'))

forward({
    from: SampleCompGate.open, //as a bonus you can call multiple effects in array like to: [fx1, fx2]
    to: myVerySideEffectFx
})

export default myVerySideEffectFx;