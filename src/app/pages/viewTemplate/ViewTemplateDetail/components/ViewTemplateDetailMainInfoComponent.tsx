import {useStore} from "effector-react";
import {Box, LinearProgress} from "@material-ui/core";
import {FTLBreadcrumbs, FTLCheckbox, FTLPageHeader, FTLSelect, FTLTextField, FTLToolbar} from "ftl-uikit";
import {history} from "ftl-core";
import ViewTemplateDetailStyles from "../styles/ViewTemplateDetailStyles";
import ViewTemplateDetailStore
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/store/ViewTemplateDetailStore";
import ViewTemplateDetailEntity
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateDetailEntity";
import {FormEvent} from "react";
import ViewTemplateDetailMainInfoProps from "../props/ViewTemplateDetailMainInfoProps";


const ViewTemplateDetailMainInfoComponent = (props: ViewTemplateDetailMainInfoProps) => {
    const classes = ViewTemplateDetailStyles()

    const store = ViewTemplateDetailStore
    const formStore: ViewTemplateDetailEntity = useStore(store.store.values)
    const formErrors = useStore(store.store.errors)
    const formIsValid = useStore(store.store.isValid)
    const formMeta = useStore(store.meta)

    console.log(formErrors)


    return (
        <>
            <form noValidate id="category-form-id" onSubmit={(e: FormEvent<HTMLFormElement>) => {
                e.preventDefault()
                store.store.submit()
            }}>
                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLTextField
                        required
                        name="title"
                        label="Title"
                        value={formStore.title}
                        onChange={(e) => store.store.fields.title.set(e.target.value)}
                        error={formErrors.title && formMeta.showErrors == true ? true : false}
                        helperText={formErrors.title}
                        fullWidth
                        placeholder="Выберите тип"
                    />
                </Box>
                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLTextField
                        required
                        name="description"
                        label="description"
                        value={formStore.description}
                        onChange={(e) => store.store.fields.description.set(e.target.value)}
                        error={formErrors.description && formMeta.showErrors == true ? true : false}
                        helperText={formErrors.description}
                        fullWidth
                        placeholder="Выберите тип"
                    />
                </Box>
                <Box display="grid" mt={6} gridGap="24px" width="360px">

                    <FTLTextField
                        required
                        name="iconEmoji"
                        label="iconEmoji"
                        value={formStore.view.icon.data}
                        onChange={(e) => {
                            store.store.fields.view.set({
                                ...formStore.view,
                                icon: {
                                    ...formStore.view.icon,
                                    data: e.target.value
                                }
                            })
                        }}
                        fullWidth
                        placeholder="iconEmoji  "
                    />
                </Box>
                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLTextField
                        required
                        name="viewTitle"
                        label="viewTitle"
                        value={formStore.view.title}
                        onChange={(e) => store.store.fields.view.set({
                            ...formStore.view,
                            title: e.target.value
                        })}
                        error={formErrors.title && formMeta.showErrors == true ? true : false}
                        helperText={formErrors.title}
                        fullWidth
                        placeholder="Выберите тип"
                    />
                </Box>
                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLTextField
                        required
                        name="viewDescription"
                        label="viewDescription"
                        value={formStore.view.description}
                        onChange={(e) => store.store.fields.view.set({
                            ...formStore.view,
                            description: e.target.value
                        })}
                        error={formErrors.description && formMeta.showErrors == true ? true : false}
                        helperText={formErrors.description}
                        fullWidth
                        placeholder="Выберите тип"
                    />
                </Box>

                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLSelect
                        name="status"
                        label="status"
                        value={{
                            label: formStore.status,
                            value: formStore.status
                        }}
                        options={[
                            {
                                label: 'Draft',
                                value: 'DRAFT'
                            }, {
                                label: 'Active',
                                value: 'ACTIVE'
                            },
                        ]}
                        onChange={(e) => {
                            store.store.fields.status.set(e ? e.value : 'DRAFT')
                        }}
                    />
                </Box>

                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLSelect
                        name="ownerType"
                        label="ownerType"
                        value={{
                            label: formStore.ownerType,
                            value: formStore.ownerType
                        }}
                        options={[
                            {
                                label: 'By Admin',
                                value: 'BY_ADMIN'
                            }, {
                                label: 'Users',
                                value: 'USERS'
                            },
                        ]}
                        onChange={(e) => {
                            store.store.fields.ownerType.set(e ? e.value : 'BY_ADMIN')
                        }}
                    />

                </Box>

                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLSelect
                        name="language"
                        label="language"
                        value={{
                            label: formStore.language,
                            value: formStore.language
                        }}
                        options={[
                            {
                                label: 'English',
                                value: 'ENGLISH'
                            }, {
                                label: 'i18',
                                value: 'I18'
                            },
                        ]}
                        onChange={(e) => {
                            store.store.fields.language.set(e ? e.value : 'ENGLISH')
                        }}

                    />
                </Box>
                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLSelect
                        name="viewType"
                        label="viewType"
                        value={{
                            label: formStore.view.viewType,
                            value: formStore.view.viewType
                        }}
                        options={[
                            {
                                label: 'CUSTOM',
                                value: 'CUSTOM'
                            }, {
                                label: 'PARAMS',
                                value: 'PARAMS'
                            },
                        ]}

                        onChange={(e) => {
                            store.store.fields.view.set({
                                ...formStore.view,
                                viewType: e ? e.value : 'CUSTOM'
                            })
                        }}
                    />
                </Box>
                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLSelect
                        name="addedType"
                        label="addedType"
                        value={{
                            label: formStore.view.addedType,
                            value: formStore.view.addedType
                        }}
                        options={[
                            {
                                label: 'MANUALLY',
                                value: 'MANUALLY'
                            }, {
                                label: 'AUTO',
                                value: 'AUTO'
                            },
                        ]}
                        onChange={(e) => {
                            store.store.fields.view.set({
                                ...formStore.view,
                                addedType: e ? e.value : 'MANUALLY'
                            })
                        }}
                    />
                </Box>
                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLSelect
                        name="displayMode"
                        label="displayMode"
                        value={{
                            label: formStore.view.displayMode,
                            value: formStore.view.displayMode
                        }}
                        options={[
                            {
                                label: 'LIST',
                                value: 'LIST'
                            }, {
                                label: 'TIMELINE',
                                value: 'TIMELINE'
                            }, {
                                label: 'CONCENTRATE',
                                value: 'CONCENTRATE'
                            }

                        ]}
                        onChange={(e) => {
                            store.store.fields.view.set({
                                ...formStore.view,
                                displayMode: e ? e.value : 'LIST'
                            })
                        }}
                    />
                </Box>


                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLCheckbox
                        name="addByDefault"
                        label="addByDefault"
                        checked={formStore.addByDefault}
                        fullWidth
                        onChange={(e) => {
                            store.store.fields.addByDefault.set(e.target.checked)
                        }}
                    />
                </Box>

                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLCheckbox
                        name="addByDefault"
                        label="canEdit"
                        checked={formStore.canEdit}
                        fullWidth
                        onChange={(e) => {
                            store.store.fields.canEdit.set(e.target.checked)
                        }}
                    />
                </Box>

                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLTextField
                        required
                        name="ownerId"
                        label="OwnerId"
                        disabled={true}
                        value={formStore.ownerId}
                        onChange={(e) => store.store.fields.ownerId.set(e.target.value)}
                        error={formErrors.ownerId && formMeta.showErrors == true ? true : false}
                        helperText={formErrors.ownerId}
                        fullWidth
                        placeholder="Выберите тип"
                    />
                </Box>
                <Box display="grid" mt={6} gridGap="24px" width="360px">
                    <FTLTextField
                        required
                        name="createdFromViewId"
                        label="createdFromViewId"
                        disabled={true}
                        value={formStore.createdFromViewId}
                        onChange={(e) => store.store.fields.createdFromViewId.set(e.target.value)}
                        error={formErrors.createdFromViewId && formMeta.showErrors == true ? true : false}
                        helperText={formErrors.createdFromViewId}
                        fullWidth
                        placeholder="Выберите тип"
                    />
                </Box>
            </form>

        </>
    );
}

export default ViewTemplateDetailMainInfoComponent
