import {useStore} from "effector-react";
import {Box, Grid, LinearProgress, Table, TableBody, TableCell, TableRow, TextField} from "@material-ui/core";
import {
    FTLBreadcrumbs,
    FTLButton,
    FTLCheckbox,
    FTLDayPicker,
    FTLPageHeader,
    FTLSelect,
    FTLTextField,
    FTLToolbar
} from "ftl-uikit";
import {history} from "ftl-core";
import ViewTemplateDetailStyles from "../styles/ViewTemplateDetailStyles";
import ViewTemplateDetailStore
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/store/ViewTemplateDetailStore";
import ViewTemplateDetailEntity
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateDetailEntity";
import {FormEvent} from "react";
import ViewTemplateDetailMainInfoProps from "../props/ViewTemplateDetailMainInfoProps";
import ViewTemplateParamsStore
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/store/ViewTemplateParamsStore";
import ViewTemplateParamsStoreEntity
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateParamsStoreEntity";
import ViewParamsType, {ViewParamsTypeList} from "../../../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewParamsType";
import {viewTemplateParamsStore} from "../page/ViewTemplateDetailPage";
import ViewParamsEntity from "../../../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateParamsEntity";


const ViewTemplateDetailMainInfoComponent = () => {
    const classes = ViewTemplateDetailStyles()

    const store = viewTemplateParamsStore
    const formStore: ViewTemplateParamsStoreEntity = useStore(store.store)
    return (
        <>
            <h1>Add Param to View</h1>
            <form noValidate id="add-param-to-view" onSubmit={(e: FormEvent<HTMLFormElement>) => {
                e.preventDefault()
                store.addParamFx({idTemplate: formStore.idTemplate, param: formStore.addParams})
                return false
            }}>
                <Grid container spacing={2}>
                    <Grid item xs={3}>

                        <FTLSelect
                            name="type"
                            label="Type"
                            value={{
                                label: formStore.addParams.type.toString(),
                                value: formStore.addParams.type
                            }}
                            options={ViewParamsTypeList}
                            onChange={(e) => {
                                if (e != null) {
                                    store.onChangeAddParamEvt({name: 'type', value: e!.value})
                                }
                            }}
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <FTLTextField
                            required
                            name="valueString"
                            label="Value String"
                            value={formStore.addParams.valueString}
                            onChange={(e) => {
                                store.onChangeAddParamEvt({name: 'valueString', value: e.target.value})
                            }}
                            fullWidth
                            placeholder="string"
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            name="valueDate"
                            label="Value Date"
                            type="datetime-local"
                            defaultValue={formStore.addParams.valueDate}
                            value={formStore.addParams.valueDate}
                            // className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            fullWidth
                            onChange={(e) => {
                                store.onChangeAddParamEvt({name: 'valueDate', value: e.target.value})
                            }}
                        />

                    </Grid>
                    <Grid item xs={2}>
                        <FTLCheckbox
                            name="valueBool"
                            label="Value Bool"
                            checked={formStore.addParams.valueBool}
                            fullWidth
                            onChange={(e) => {
                                store.onChangeAddParamEvt({name: 'valueBool', value: e.target.checked})
                            }}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <FTLButton type={'submit'}>Add</FTLButton>
                    </Grid>
                </Grid>
            </form>
            <h1>Params</h1>
            <Grid container spacing={2}>
                <Grid item xs={12}>

                    <Table>
                        <TableBody>
                            {formStore.paramsList.map((param) => {
                                return (<TableRow>
                                    <TableCell>
                                        <FTLSelect
                                            name="type"
                                            label="Type"
                                            value={{
                                                label: param.type.toString(),
                                                value: param.type
                                            }}
                                            options={ViewParamsTypeList}
                                            onChange={(e) => {
                                                if (e != null) {
                                                    store.onChangeParamEvt({
                                                        id: param.id,
                                                        name: 'type',
                                                        value: e!.value
                                                    })
                                                }
                                            }}
                                        />

                                    </TableCell>
                                    <TableCell>
                                        <FTLTextField
                                            required
                                            name="valueString"
                                            label="Value String"
                                            value={param.valueString}
                                            onChange={(e) => {
                                                store.onChangeParamEvt({
                                                    id: param.id,
                                                    name: 'valueString',
                                                    value: e.target.value
                                                })
                                            }}
                                            fullWidth
                                            placeholder="string"
                                        /></TableCell>
                                    <TableCell>
                                        <TextField
                                            name="valueDate"
                                            label="Value Date"
                                            type="datetime-local"
                                            defaultValue={param.valueDate}
                                            value={param.valueDate}
                                            // className={classes.textField}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            fullWidth
                                            onChange={(e) => {
                                                store.onChangeParamEvt({
                                                    id: param.id,
                                                    name: 'valueDate',
                                                    value: e.target.value
                                                })
                                            }}
                                        />
                                    </TableCell>
                                    <TableCell>
                                        <FTLCheckbox
                                            name="valueBool"
                                            label="Value Bool"
                                            checked={param.valueBool}
                                            fullWidth
                                            onChange={(e) => {
                                                store.onChangeParamEvt({
                                                    id: param.id,
                                                    name: 'valueBool',
                                                    value: e.target.checked
                                                })
                                            }}
                                        />
                                    </TableCell>
                                    <TableCell>
                                        <FTLButton onClick={() => {
                                            store.editParamFx({
                                                idTemplate: formStore.idTemplate, param: param
                                            })
                                        }}>edit</FTLButton>
                                        <FTLButton onClick={() => {
                                            store.deleteParamFx({
                                                idTemplate: formStore.idTemplate, id: param.id
                                            })
                                        }}>delete</FTLButton>
                                    </TableCell>
                                </TableRow>)
                            })}
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
        </>
    );
}

export default ViewTemplateDetailMainInfoComponent
