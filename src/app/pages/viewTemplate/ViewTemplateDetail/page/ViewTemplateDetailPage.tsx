import {useGate, useStore} from "effector-react";
import {Box, LinearProgress} from "@material-ui/core";
import {FTLBreadcrumbs, FTLButton, FTLPageHeader, FTLTab, FTLTabs, FTLToolbar} from "ftl-uikit";
import {history} from "ftl-core";
import {useParams} from "react-router-dom"
import ViewTemplateDetailProps from "../props/ViewTemplateDetailProps";
import ViewTemplateDetailStyles from "../styles/ViewTemplateDetailStyles";
import ViewTemplateDetailStore
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/store/ViewTemplateDetailStore";
import ViewTemplateDetailEntity
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/entity/ViewTemplateDetailEntity";
import {useState} from "react";
import ViewTemplateDetailMainInfoComponent from "../components/ViewTemplateDetailMainInfoComponent";
import ViewTemplateAdminApiRepository
    from "../../../../../infrastructure/api/viewTemplate/ViewTemplateAdminApiRepository";
import ViewTemplateDetailParamsComponent from "../components/ViewTemplateDetailParamsComponent";
import ViewTemplateParamsStore
    from "../../../../../domains/viewTemplate/ViewTemplateDetail/store/ViewTemplateParamsStore";

const breadcrumbsItems = [
    {label: "View Template", to: "/view-template"},
    {label: `Просмотр`, to: "#",},
]

export const viewTemplateParamsStore = ViewTemplateParamsStore()

const ViewTemplateDetailPage = (props: ViewTemplateDetailProps) => {
    const classes = ViewTemplateDetailStyles()
    const {id} = useParams<{ id?: string }>()
    let entityId: string | undefined = props.id ? props.id : id

    const store = ViewTemplateDetailStore
    useGate(store.gate, entityId)
    const formStore: ViewTemplateDetailEntity = useStore(store.store.values)
    const formErrors = useStore(store.store.errors)
    const formIsValid = useStore(store.store.isValid)
    const formMeta = useStore(store.meta)

    useGate(viewTemplateParamsStore.gate, entityId)

    console.log(formErrors)

    const [tab, setTab] = useState(0)

    return (
        <div className={classes.root}>
            <Box marginBottom="auto">
                <FTLBreadcrumbs items={breadcrumbsItems}/>
                {formMeta.isLoad ? <LinearProgress color={'secondary'}/> :
                    <div style={
                        {width: '100%', height: '4px'}
                    }></div>}
                <FTLPageHeader title={`View Template - ${formStore.title}`} BoxProps={
                    {mb: 6}
                }/>
                <FTLTabs value={tab} onChange={(e, num) => setTab(num)}>
                    <FTLTab label="Info"/>
                    <FTLTab label="Params"/>
                </FTLTabs>

                {tab == 0 && <ViewTemplateDetailMainInfoComponent/>}
                {tab == 1 && <ViewTemplateDetailParamsComponent/>}
            </Box>



            <FTLToolbar
                position="sticky"
                onSaveBtnId="form"
                disabled={formIsValid == false || formMeta.isLoad == true}
                onSaveMessage="Сохранить"
                onCancel={() => {
                    history.goBack()
                }}
                hasCheckbox={true}
                showSaveButton={true}
                onSave={() => {
                    store.store.submit()
                }}
                // currentTabIndex={2}
            >
                <FTLButton onClick={async () => {
                    let result = await ViewTemplateAdminApiRepository.bulkAddByAdmin(formStore)
                    alert(result.result)
                }}>Bulk add to all users</FTLButton>
            </FTLToolbar>
        </div>
    );
}

export default ViewTemplateDetailPage
