// @ts-ignore
import React, {ReactNode, useState} from "react";
import {Box} from "@material-ui/core";
import {FTLPageHeader, FTLPureTableComponent, FTLSearchField} from "ftl-uikit";
import {useGate, useStore} from "effector-react";
import {history, useShortcut} from "ftl-core";
import ViewTemplateListProps from "../props/ViewTemplateListProps";
import ViewTemplateListStore from "../../../../../domains/viewTemplate/ViewTemplateList/store/ViewTemplateListStore";


const ViewTemplateListPage = ({}: ViewTemplateListProps) => {

    const store = ViewTemplateListStore
    useGate(store.gate)

    const smartTableStore = useStore(store.store)

    useShortcut("goBack", history.goBack)

    return (
        <React.Fragment>
            <FTLPageHeader
                title="View Template"
                buttons={[
                    {
                        children: "Add template",
                        onClick: () => history.push("/view-template/new"),
                    },
                ]}
            />
            <Box display="flex" pt={6} overflow="auto" flexShrink={0}>
                <Box width="200px" mr={4} mb={4} flexShrink="0">
                    <FTLSearchField onChange={(value: string) => store.onChangeSearchParamEvt({
                        key: "query",
                        value: value
                    })}/>
                </Box>
            </Box>
            <FTLPureTableComponent
                {...smartTableStore}
                onChangePage={store.onChangeCurrentPageEvt}
                onChangeRowsPerPage={store.onChangePageSizeEvt}
                onClickRow={(data => history.push(`/view-template/${data.id}`))}
                onClickHeaderCell={(meta) => {
                    store.onChangeSortEvt({
                        key: meta.id,
                        value: meta.sortDirection == "asc" ? "desc" : "asc"
                    })
                }}
            />
        </React.Fragment>
    );
}

export default ViewTemplateListPage


