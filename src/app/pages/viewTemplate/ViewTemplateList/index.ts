import ViewTemplateListPage from "./page/ViewTemplateListPage";

export type {default as FTLCredentialListProps} from './props/ViewTemplateListProps'

export default ViewTemplateListPage;
