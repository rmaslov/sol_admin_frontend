import {makeStyles} from "@material-ui/core";

const ViewTemplateListStyle = makeStyles({
  maxWidthXs: {
    maxWidth: 280,
  },
})

export default ViewTemplateListStyle
