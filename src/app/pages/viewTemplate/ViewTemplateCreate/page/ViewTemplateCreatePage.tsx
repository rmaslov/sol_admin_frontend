import {useGate, useStore} from "effector-react";
import {Box, LinearProgress} from "@material-ui/core";
import {FTLBreadcrumbs, FTLPageHeader, FTLSelect, FTLTextField, FTLToolbar} from "ftl-uikit";
import {history, ListResponse, SelectOption} from "ftl-core";
import ViewTemplateCreateEntity
    from "../../../../../domains/viewTemplate/ViewTemplateCreate/entity/ViewTemplateCreateEntity";
import {FormEvent} from "react";
import ViewTemplateCreateStyles from "../styles/ViewTemplateCreateStyles";
import ViewTemplateCreateStore
    from "../../../../../domains/viewTemplate/ViewTemplateCreate/store/ViewTemplateCreateStore";
import ViewTemplateListApiRepository
    from "../../../../../infrastructure/api/viewTemplate/ViewTemplateListApiRepository";
import {AxiosRequestConfig, AxiosResponse} from "axios";
import ViewTemplateListEntity from "../../../../../domains/viewTemplate/ViewTemplateList/entity/ViewTemplateListEntity";

const breadcrumbsItems = [
    {label: "View Template", to: "/view-template"},
    {label: `Создание`, to: "#",},
]

const ViewTemplateCreatePage = () => {
    const classes = ViewTemplateCreateStyles()
    const store = ViewTemplateCreateStore
    useGate(store.gate)
    const formStore: ViewTemplateCreateEntity = useStore(store.store.values)
    const formErrors = useStore(store.store.errors)
    const formIsValid = useStore(store.store.isValid)
    const formMeta = useStore(store.meta)

    store.didSendData.watch((payload) => {
        history.push(`/view-template/${payload.id}`)
    })


    return (
        <div className={classes.root}>
            <Box marginBottom="auto">
                <FTLBreadcrumbs items={breadcrumbsItems}/>
                {formMeta.isLoad ? <LinearProgress color={'secondary'}/> :
                    <div style={
                        {width: '100%', height: '4px'}
                    }></div>}

                <FTLPageHeader title={`Create ViewTemplate`} BoxProps={
                    {
                        mb: 6
                    }
                }/>

                <form noValidate id="category-form-id" onSubmit={(e: FormEvent<HTMLFormElement>) => {
                    e.preventDefault()
                    store.store.submit()
                }}>
                    <Box display="grid" mt={6} gridGap="24px" width="360px">
                        <FTLTextField
                            required
                            name="title"
                            label="Title"
                            value={formStore.title}
                            onChange={(e) => store.store.fields.title.set(e.target.value)}
                            error={formErrors.title && formMeta.showErrors == true ? true : false}
                            helperText={formErrors.title}
                            fullWidth
                            placeholder="title"
                        />
                    </Box>
                    <Box display="grid" mt={6} gridGap="24px" width="360px">
                        <FTLTextField
                            required
                            name="description"
                            label="description"
                            value={formStore.description}
                            onChange={(e) => store.store.fields.description.set(e.target.value)}
                            error={formErrors.description && formMeta.showErrors == true ? true : false}
                            helperText={formErrors.description}
                            fullWidth
                            placeholder="description"
                        />
                    </Box>
                </form>
            </Box>
            <FTLToolbar
                position="sticky"
                onSaveBtnId="form"
                disabled={formIsValid == false || formMeta.isLoad == true}
                onSaveMessage="Сохранить"
                onCancel={() => {
                    history.goBack()
                }}
                hasCheckbox={true}
                showSaveButton={true}
                onSave={() => {
                    store.store.submit()
                }}
            >
            </FTLToolbar>
        </div>
    );
}

export default ViewTemplateCreatePage
